# module "aws_vpc" {
#   source = "./../vpc"
# }

# data "aws_vpc" "ec2_jenkins_vpc" {
#   filter {
#     name   = "tag:Name"
#     values = ["ec2_jenkins_inceptions"]
#   }
# }

resource "aws_security_group" "ec2_jenkins_sg" {
  name        = "ec2_jenkins-sg"
  description = "ec2_jenkins security group"
  # vpc_id      = module.aws_vpc.vpc_id
  vpc_id      = var.vpc_id

  ingress {
    description = "Allow all traffic through port 8080"
    from_port   = "8080"
    to_port     = "8080"
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
    # cidr_blocks = ["91.227.212.3/32"] # VPN
  }

  ingress {
    description = "Allow SSH from my computer - default all traffic accepted"
    from_port   = "22"
    to_port     = "22"
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
    # cidr_blocks = ["91.227.212.3/32"] # VPN
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "ec2_jenkins-sg"
  }
}