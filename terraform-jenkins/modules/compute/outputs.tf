output "public_ip" {
  description = "The public IP address of the Jenkins server"
  value       = aws_instance.ec2_jenkins_node.public_ip
}

output "public_key" {
  value = var.public_key
}