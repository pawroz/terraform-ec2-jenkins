# data "aws_ami" "server_ami" {
#   most_recent = true
#   owners      = ["137112412989"]

#   filter {
#     name   = "name"
#     values = ["amzn2-ami-kernel-5.10-hvm-2.0.*.1-x86_64-gp2"]
#   }
# }

# data "aws_security_group" "ec2_jenkins_sg" {
#   filter {
#     name   = "tag:Name"
#     values = ["ec2_jenkins-sg"]
#   }
# }

# data "aws_subnet" "ec2_jenkins_public_subnet" {
#   filter {
#     name   = "tag:Name"
#     values = ["ec2_jenkins-public"]
#   }
# }

data "aws_ami" "server_ami" {
  most_recent = true
  owners      = ["137112412989"]

  filter {
    name   = "name"
    values = ["amzn2-ami-kernel-5.10-hvm-2.0.*.1-x86_64-gp2"]
  }
}

resource "aws_instance" "ec2_jenkins_node" {
  instance_type = "t2.micro"
  ami           = data.aws_ami.server_ami.id
  vpc_security_group_ids = [var.ec2_jenkins_sg_id]
  subnet_id = var.ec2_jenkins_public_subnet_id
  user_data = <<-EOF
    #!/bin/bash
    sudo apt update -y
    sudo apt install -y yum-utils
    sudo yum update –y
    sudo wget -O /etc/yum.repos.d/jenkins.repo https://pkg.jenkins.io/redhat-stable/jenkins.repo
    sudo rpm --import https://pkg.jenkins.io/redhat-stable/jenkins.io.key
    sudo yum upgrade
    sudo amazon-linux-extras install java-openjdk11 -y
    sudo yum install jenkins -y
    sudo systemctl enable jenkins
    sudo systemctl start jenkins
    EOF

  tags = {
    Name = "ec2_jenkins-node"
  }

  # provisioner "file" {
  #   source      = "install_jenkins.sh"
  #   destination = "/tmp/install_jenkins.sh"
  # }

}

# resource "aws_key_pair" "pr_auth" {
#   key_name = "prkey_new"
  # public_key = file("~/.ssh/mtckey.pub")
  # public_key = file("~/my_stuff/terraform-ec2-jenkins/pr_new.pub")
  # public_key = file(var.public_key_path)
#   public_key = var.public_key
# }