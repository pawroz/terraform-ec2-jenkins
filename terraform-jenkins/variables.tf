variable "aws_region" {
  default = "us-east-1a"
}

variable "vpc_cidr_block" {
  description = "CIDR block for VPC"
  type        = string
}

variable "public_subnet_cidr_block" {
  description = "CIDR block for public subnet"
  type        = string
}

variable "public_key_path" {
  type        = string
  description = "Path to the public key file"
  default     = "~/my_stuff/terraform-ec2-jenkins/pr_new.pub"
}

variable "owner" {
  type        = string
  description = "(Optional) Project Owner. Defaults to Terraform"
  default     = "Pablo"
}

variable "environment" {
  type        = string
  description = "Application environment for deployment."
  default     = "development"
}

variable "region" {
  type        = string
  description = "(Optional) The region where the resources are created. Defaults to us-east-1."
  default     = "us-east-1"
}

variable "public_key" {
  type        = string
  description = "Public key for SSH access"
  default     = ""
}

# variable "my_ip" {
#    description = "None"
#    type = string
#    sensitive = true
# }
