variable "public_key_path" {
  type        = string
  description = "Path to the public key file"
  default     = "~/my_stuff/terraform-ec2-jenkins/pr_new.pub"
}

variable "private_key_path" {
  type        = string
  description = "Path to the public key file"
  default     = "~/my_stuff/terraform-ec2-jenkins/pr_new.pem"
}

# variable "server_ami" {
#   description = "Specific kind of amazon machine image - kernel version"
# }

variable "ec2_jenkins_sg_id" {
  description = "ID of security group of the ec2."
}

variable "ec2_jenkins_public_subnet_id" {
  description = "Subnet of the ec2."
}

variable "owner" {
  type        = string
  description = "(Optional) Project Owner. Defaults to Terraform"
  default     = "Pablo"
}

variable "environment" {
  type        = string
  description = "Application environment for deployment."
  default     = "development"
}

variable "region" {
  type        = string
  description = "(Optional) The region where the resources are created. Defaults to us-east-1."
  default     = "us-east-1"
}

variable "public_key" {
  type        = string
  description = "Public key for SSH access"
  default     = ""
}