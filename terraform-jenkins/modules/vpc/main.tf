# Data store that holds the available AZs in our region
data "aws_availability_zones" "available" {
  state = "available"
}


resource "aws_vpc" "ec2_jenkins_vpc" {
  cidr_block           = var.vpc_cidr_block
  enable_dns_hostnames = true
  enable_dns_support   = true

  tags = {
    Name = "ec2_jenkins_inceptions"
  }
}

resource "aws_subnet" "ec2_jenkins_public_subnet" {
  vpc_id                  = aws_vpc.ec2_jenkins_vpc.id
  cidr_block              = var.subnet_cidr_block
  map_public_ip_on_launch = true
  availability_zone       = "us-east-1a"

  tags = {
    Name = "ec2_jenkins-public"
  }
}

resource "aws_internet_gateway" "ec2_jenkins_internet_gateway" {
  vpc_id = aws_vpc.ec2_jenkins_vpc.id

  tags = {
    Name = "ec2_jenkins-igw"
  }
}

resource "aws_route_table" "ec2_jenkins_public_rt" {
  vpc_id = aws_vpc.ec2_jenkins_vpc.id

  tags = {
    Name = "ec2_jenkins-rt"
  }
}

# okresla trasy, które mają być dodane do tablicy trasowania
resource "aws_route" "default_route" {
  route_table_id         = aws_route_table.ec2_jenkins_public_rt.id
  destination_cidr_block = "0.0.0.0/0"
  gateway_id             = aws_internet_gateway.ec2_jenkins_internet_gateway.id

}

resource "aws_route_table_association" "ec2_jenkins_public_assoc" {
  subnet_id      = aws_subnet.ec2_jenkins_public_subnet.id
  route_table_id = aws_route_table.ec2_jenkins_public_rt.id

}