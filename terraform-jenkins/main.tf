module "vpc" {
  source = "./modules/vpc"
}

module "security_group" {
  source = "./modules/security_group"
  vpc_id = module.vpc.vpc_id
}

module "ec2_node_with_jenkins" {
  source                       = "./modules/compute"
  ec2_jenkins_sg_id            = module.security_group.sg_id
  ec2_jenkins_public_subnet_id = module.vpc.public_subnet_id
}
