terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.0"
    }
  }
}

provider "aws" {
  region = var.region
  default_tags {
    tags = {
      Owner       = var.owner
      Environment = var.environment
    }
  }
  # shared_credentials_files = ["~/my_stuff/terraform-ec2-jenkins/credentials"]
  # profile                  = "vscode"
}

