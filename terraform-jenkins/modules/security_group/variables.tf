variable "owner" {
  type        = string
  description = "(Optional) Project Owner. Defaults to Terraform"
  default     = "Pablo"
}

variable "environment" {
  type        = string
  description = "Application environment for deployment."
  default     = "development"
}

variable "region" {
  type        = string
  description = "(Optional) The region where the resources are created. Defaults to us-east-1."
  default     = "us-east-1"
}

variable "vpc_id" {
  description = "ID of the VPC where the security group will be created."
}