output "vpc_id" {
  description = "ID of the VPC"
  value       = module.vpc.vpc_id
}

output "public_subnet_id" {
  description = "ID of the public subnet"
  value       = module.vpc.public_subnet_id
}

output "security_group_id" {
  description = "The ID of the security group"
  value       = module.security_group.sg_id
}

output "public_key" {
  value = var.public_key
}